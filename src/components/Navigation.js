import React from "react";
import { FaCheckCircle, FaUserCircle } from "react-icons/fa";
import { RiSettings5Fill } from "react-icons/ri";
import { HiOutlineDocumentReport } from "react-icons/hi";

export default function Navigation({ setOpenSetting }) {
  return (
    <nav className="flex justify-between pt-5 text-white w-11/12 mx-auto flex-wrap">
      <div className="flex items-center gap-1 cursor-pointer text-xl pr-4">
        <FaCheckCircle />
        <h1 className="font-semibold">Pomofocus</h1>
      </div>
      <span className="flex gap-5">
        <button className="flex  items-center gap-1 cursor-pointer">
          <HiOutlineDocumentReport />
          <div>Report</div>
        </button>
        <button className="flex  items-center gap-1 cursor-pointer">
          <RiSettings5Fill onClick={() => setOpenSetting((value) => !value)} />
          <div>Settings</div>
        </button>
        <button className="flex  items-center gap-1 cursor-pointer">
          <FaUserCircle />
          <div>Login</div>
        </button>
      </span>
    </nav>
  );
}
